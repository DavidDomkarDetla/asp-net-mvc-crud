﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRUD.Models
{
    public enum Platform
    {
        Mobile,
        Web,
        Server
    }

    public class Framework
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Platform Platform { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}