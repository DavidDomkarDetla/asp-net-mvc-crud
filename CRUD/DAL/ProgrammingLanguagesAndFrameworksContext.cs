﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using CRUD.Models;

namespace CRUD.DAL
{
    public class ProgrammingLanguagesAndFrameworksContext : DbContext
    {
        public ProgrammingLanguagesAndFrameworksContext() : base("ProgrammingLanguagesAndFrameworksContext")
        {

        }

        public DbSet<ProgrammingLanguage> ProgrammingLanguages { get; set; }
        public DbSet<Framework> Frameworks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}