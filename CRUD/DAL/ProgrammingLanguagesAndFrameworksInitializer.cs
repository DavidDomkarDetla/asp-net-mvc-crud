﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using CRUD.Models;

namespace CRUD.DAL
{
    public class ProgrammingLanguagesAndFrameworksInitializer : DropCreateDatabaseIfModelChanges<ProgrammingLanguagesAndFrameworksContext>
    {
        protected override void Seed(ProgrammingLanguagesAndFrameworksContext context)
        {
            var programmingLanguages = new List<ProgrammingLanguage>
            {
                new ProgrammingLanguage{Name="JavaScript",Description="Programming language of HTML and the Web. It is high-level, just-in-time compiled, multi-paradigm and conforms to the ECMAScript specification.",ReleaseDate=DateTime.Parse("1995-12-04")},
                new ProgrammingLanguage{Name="TypeScript",Description="Open-source programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript",ReleaseDate=DateTime.Parse("2012-10-1")},
                new ProgrammingLanguage{Name="Dart",Description="Client-optimized language for fast apps on any platform.",ReleaseDate=DateTime.Parse("2011-10-10")},
                new ProgrammingLanguage{Name="Rust",Description="A language empowering everyone to build reliable and efficient software.",ReleaseDate=DateTime.Parse("2010-07-07")},
                new ProgrammingLanguage{Name="C#",Description="General-purpose, multi-paradigm programming language encompassing strong typing, lexically scoped, imperative, declarative, functional, generic, object-oriented (class-based), and component-oriented programming disciplines.",ReleaseDate=DateTime.Parse("2000-01-01")},
            };

            programmingLanguages.ForEach(s => context.ProgrammingLanguages.Add(s));
            context.SaveChanges();

            var frameworks = new List<Framework>
            {
                new Framework{Name="Vue",Description="Progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable.",Platform=Platform.Web,ReleaseDate=DateTime.Parse("2014-02-01")},
                new Framework{Name="React",Description="JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies.",Platform=Platform.Web,ReleaseDate=DateTime.Parse("2013-05-29")},
                new Framework{Name="Flutter",Description="Flutter is Google's UI toolkit for crafting beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.",Platform=Platform.Mobile,ReleaseDate=DateTime.Parse("2017-05-01")},
                new Framework{Name="Express",Description="Web application framework for Node.js, released as free and open-source software under the MIT License.",Platform=Platform.Server,ReleaseDate=DateTime.Parse("2010-11-16")},
                new Framework{Name="Rocket",Description="Web framework for Rust that makes it simple to write fast, secure web applications without sacrificing flexibility, usability, or type safety.",Platform=Platform.Server,ReleaseDate=DateTime.Parse("2016-01-01")},
            };

            frameworks.ForEach(s => context.Frameworks.Add(s));
            context.SaveChanges();
        }
    }
}