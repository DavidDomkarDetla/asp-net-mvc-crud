﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRUD.DAL;
using CRUD.Models;

namespace CRUD.Controllers
{
    public class ProgrammingLanguageController : Controller
    {
        private ProgrammingLanguagesAndFrameworksContext db = new ProgrammingLanguagesAndFrameworksContext();

        // GET: ProgrammingLanguage
        public async Task<ActionResult> Index()
        {
            return View(await db.ProgrammingLanguages.ToListAsync());
        }

        // GET: ProgrammingLanguage/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgrammingLanguage programmingLanguage = await db.ProgrammingLanguages.FindAsync(id);
            if (programmingLanguage == null)
            {
                return HttpNotFound();
            }
            return View(programmingLanguage);
        }

        // GET: ProgrammingLanguage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProgrammingLanguage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Description,ReleaseDate")] ProgrammingLanguage programmingLanguage)
        {
            if (ModelState.IsValid)
            {
                db.ProgrammingLanguages.Add(programmingLanguage);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(programmingLanguage);
        }

        // GET: ProgrammingLanguage/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgrammingLanguage programmingLanguage = await db.ProgrammingLanguages.FindAsync(id);
            if (programmingLanguage == null)
            {
                return HttpNotFound();
            }
            return View(programmingLanguage);
        }

        // POST: ProgrammingLanguage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Description,ReleaseDate")] ProgrammingLanguage programmingLanguage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(programmingLanguage).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(programmingLanguage);
        }

        // GET: ProgrammingLanguage/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgrammingLanguage programmingLanguage = await db.ProgrammingLanguages.FindAsync(id);
            if (programmingLanguage == null)
            {
                return HttpNotFound();
            }
            return View(programmingLanguage);
        }

        // POST: ProgrammingLanguage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProgrammingLanguage programmingLanguage = await db.ProgrammingLanguages.FindAsync(id);
            db.ProgrammingLanguages.Remove(programmingLanguage);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
