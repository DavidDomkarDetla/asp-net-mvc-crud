﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRUD.DAL;
using CRUD.Models;

namespace CRUD.Controllers
{
    public class FrameworkController : Controller
    {
        private ProgrammingLanguagesAndFrameworksContext db = new ProgrammingLanguagesAndFrameworksContext();

        // GET: Framework
        public async Task<ActionResult> Index()
        {
            return View(await db.Frameworks.ToListAsync());
        }

        // GET: Framework/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Framework framework = await db.Frameworks.FindAsync(id);
            if (framework == null)
            {
                return HttpNotFound();
            }
            return View(framework);
        }

        // GET: Framework/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Framework/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Description,Platform,ReleaseDate")] Framework framework)
        {
            if (ModelState.IsValid)
            {
                db.Frameworks.Add(framework);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(framework);
        }

        // GET: Framework/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Framework framework = await db.Frameworks.FindAsync(id);
            if (framework == null)
            {
                return HttpNotFound();
            }
            return View(framework);
        }

        // POST: Framework/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Description,Platform,ReleaseDate")] Framework framework)
        {
            if (ModelState.IsValid)
            {
                db.Entry(framework).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(framework);
        }

        // GET: Framework/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Framework framework = await db.Frameworks.FindAsync(id);
            if (framework == null)
            {
                return HttpNotFound();
            }
            return View(framework);
        }

        // POST: Framework/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Framework framework = await db.Frameworks.FindAsync(id);
            db.Frameworks.Remove(framework);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
